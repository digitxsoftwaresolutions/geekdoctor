:: # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
:: #                                                               #
:: # This script sets up the client machine with OEM settings      #
:: # for Office 2016 running on Windows 10 RS3. 		   #
:: # This script is meant to be run on the client  		   #
:: # machine with Office preinstalled by the OEM. 	           #
:: #                                                               #
:: # For more information, please read the documentation.          #
:: #                                                               #
:: # Date created: July 2017                                        #
:: #                                                               #
:: # Copyright Microsoft Corporation 2017.                         #
:: #                                                               #
:: # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

@echo off

setLocal
cls

set str=%~n0
for /f "tokens=1,2,3 delims=." %%a in ("%str%") do set oobeMode=%%b& set referral=%%c

set wow=true

::
:: Taking care of arguments
::

:startArgs
if "%1"=="" goto endArgs
set key=%1
set value=%2
set setVal=0
if /I "%key%"=="Mode" (set oobeMode=%value%&set setVal=2
) else (
if /I "%key%"=="Referral" (set referral=%value%&set setVal=2
) else (
if NOT "%value%"=="1" (exit /B 1)
))
if %setVal%==0 (exit /B 1)
if %setVal%==2 shift
shift
goto startArgs
:endArgs

::
:: Validate the Mode parameter is supported
::

if /I "%oobeMode%"  NEQ "" (
	if /I "%oobeMode%" NEQ "OEM" (
		if /I "%oobeMode%"  NEQ "OEMTA" (
			if /I "%oobeMode%"  NEQ "OEMPIPC" (
				if /I "%oobeMode%" NEQ "OEMPIPCHYBRID" (
					if /I "%oobeMode%" NEQ "OEMSMB" (
						if /I "%oobeMode%" NEQ "CLOUDBOOK" (
							echo Invalid Mode parameter passed in: %oobemode%&echo Invalid Mode parameter passed in: %oobemode% >>%TEMP%\oobe.log&exit /B 1
						)
					)
				)
			)
		)
	)
)

::
:: Display selected options
::

if /I "%oobeMode%" NEQ "" (
	echo Mode = %oobeMode%&echo Mode = %oobeMode% >>%TEMP%\oobe.log
) else (
	echo Mode = OEM&echo Mode = OEM >>%TEMP%\oobe.log
)

if /I "%oobeMode%" NEQ "OEMPIPC" (
	if /I "%oobeMode%" NEQ "OEMPIPCHYBRID" (
		if /I "%oobeMode%" NEQ "OEMSMB" (
			if NOT "%referral%"=="" (echo Referral = %referral%& echo echo Referral = %referral% >>%TEMP%\oobe.log) else (echo No Referral code given&echo No Referral code given >>%TEMP%\oobe.log)
)))

::
:: Add the HardwareId
::

call :generateGUID GUID

::Add to the virtual registry location
reg add HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\16.0\Common\OEM /v OOBE /t REG_SZ /d %GUID%  /f >>%TEMP%\oobe.log

if "%PROCESSOR_ARCHITECTURE%" NEQ "x86" (
	reg add HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Office\16.0\Common\OEM /v OOBE /t REG_SZ /d %GUID% /f >>%TEMP%\oobe.log
)

::
:: Add the OOBEMode - default is OEM
::

::Add to the virtual registry location
if /I "%oobeMode%"=="" (
	reg add HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\16.0\Common\OEM /v OOBEMode /t REG_SZ /d OEM /f >>%TEMP%\oobe.log

	if /I "%PROCESSOR_ARCHITECTURE%" NEQ "x86" (
		reg add HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Office\16.0\Common\OEM /v OOBEMode /t REG_SZ /d OEM /f >>%TEMP%\oobe.log
	)
) else (
	reg add HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\16.0\Common\OEM /v OOBEMode /t REG_SZ /d %oobeMode% /f >>%TEMP%\oobe.log

	if /I "%PROCESSOR_ARCHITECTURE%" NEQ "x86" (
		reg add HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Office\16.0\Common\OEM /v OOBEMode /t REG_SZ /d %oobeMode% /f >>%TEMP%\oobe.log
	)
)

::
:: Add Cloudbook key
::

::Add to the virtual registry location
reg add HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\C20CDB00F /t REG_SZ /f >>%TEMP%\oobe.log

if /I "%PROCESSOR_ARCHITECTURE%" NEQ "x86" (
	reg add HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\C20CDB00F /t REG_SZ /f >>%TEMP%\oobe.log
)

::
:: Add the ReferralData
::

::Add to the virtual registry location
if /I "%oobeMode%" NEQ "OEMPIPC" (
	if /I "%oobeMode%" NEQ "OEMPIPCHYBRID" (
		if /I "%oobeMode%" NEQ "OEMSMB" (
			if "%referral%" NEQ "" (
				reg add HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\OfficeSoftwareProtectionPlatform /v ReferralData /t REG_DWORD /d "%referral%" /f >>%TEMP%\oobe.log

				if /I "%PROCESSOR_ARCHITECTURE%" NEQ "x86" (
					reg add HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\OfficeSoftwareProtectionPlatform /v ReferralData /t REG_DWORD /d "%referral%" /f >>%TEMP%\oobe.log
				)
			)
		)
	)
)

::
:: Update Registry Permissions
::

echo HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\16.0\Common\OEM [1 5 7 11 17] >>%TEMP%\regsettings.txt
echo HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\OfficeSoftwareProtectionPlatform [1 5 7 11 17] >>%TEMP%\regsettings.txt
echo HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\C20CDB00F [1 5 7 11 17] >>%TEMP%\regsettings.txt

if /I "%PROCESSOR_ARCHITECTURE%" NEQ "x86" (
	echo HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Office\16.0\Common\OEM [1 5 7 11 17] >>%TEMP%\regsettings.txt
	echo HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\OfficeSoftwareProtectionPlatform [1 5 7 11 17] >>%TEMP%\regsettings.txt
	echo HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\C20CDB00F [1 5 7 11 17] >>%TEMP%\regsettings.txt
)

regini %TEMP%\regsettings.txt
del %TEMP%\regsettings.txt

goto :eof

:GenerateGUID <return>
setlocal enabledelayedexpansion
set "fullGUID=" 
for /L %%n in (1,1,32) do ( 
	if "%%~n"=="9"  set "fullGUID=!fullGUID!-" 
	if "%%~n"=="13" set "fullGUID=!fullGUID!-" 
	if "%%~n"=="17" set "fullGUID=!fullGUID!-" 
	if "%%~n"=="21" set "fullGUID=!fullGUID!-" 

	set /a "currentDigit=!random! %% 16" 

	if "!currentDigit!"=="10" set "currentDigit=A" 
	if "!currentDigit!"=="11" set "currentDigit=B" 
	if "!currentDigit!"=="12" set "currentDigit=C" 
	if "!currentDigit!"=="13" set "currentDigit=D" 
	if "!currentDigit!"=="14" set "currentDigit=E" 
	if "!currentDigit!"=="15" set "currentDigit=F" 

	set "fullGUID=!fullGUID!!currentDigit!" 
) 
set "fullGUID={!fullGUID!}
endlocal & if not "%~1"=="" set "%~1=%fullGUID%" 
goto :eof