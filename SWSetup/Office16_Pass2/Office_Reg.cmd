@ECHO OFF
set LogFile=C:\System.sav\logs\MSOffice16_Registry.log
pushd C:\SWSetup\Office16_Pass2
REM Masaki solution for CNB/CDT detection
SET dpsPath=
SET mlPrefix=
SET BountyCode=
FOR /F %%a IN ('dir C:\system.sav\*_DPS /b') DO SET dpsPath=C:\system.sav\%%a
if not defined dpsPath echo dpsPath not found >>%LogFile% & goto RESULTFAILED
ECHO [%time%][%~nx0] DPS File : %dpsPath% >>%LogFile%
FOR /F "tokens=2 delims==" %%a IN ('find /n /i "ML_Prefix" %dpsPath%') DO SET mlPrefix=%%a
if not defined mlPrefix echo mlPrefix not found >>%LogFile% & goto RESULTFAILED
ECHO [%time%][%~nx0] %mlPrefix% >>%LogFile%

for /f "tokens=1,2 usebackq delims='='" %%a in (C:\HP\Bin\RStone.INI) do (
    if "%%a" == "BrandName" set BRAND=%%b
    if "%%a" == "OSSkuFlag" set OSSKU=%%b
    if "%%a" == "Family" set FamilyCode=%%b
    if "%%a" == "ImageIDFlag" set ImageID=%%b
    if "%%a" == "OSSubversionFlag" set OSFlag=%%b
    if "%%a" == "MSOfficeFlag" set OfficeOA3=%%b
    if "%%a" == "SKUOptionConfig" set SKUOP=%%b
)

For /f "tokens=1,2 usebackq delims='='" %%a in (c:\system.sav\util\VARS.ini) do (
    If "%%a" == "SEG.ENT"  set SEG.ENT=%%b
    If "%%a" == "SEG.SBSO" set SEG.SBSO=%%b
    If "%%a" == "SEG.CON"  set SEG.CON=%%b
    If "%%a" == "SEG.PRM"  set SEG.PRM=%%b
    If "%%a" == "SEG.WKS"  set SEG.WKS=%%b
    If "%%a" == "SEG.RPS"  set SEG.RPS=%%b
    If "%%a" == "SEG.TC"   set SEG.TC=%%b
    If "%%a" == "CT.AIO"   set CT.AIO=%%b
    If "%%a" == "CT.MT"    set CT.MT=%%b
    If "%%a" == "CT.NB"    set CT.NB=%%b
    If "%%a" == "CT.TC"    set CT.TC=%%b
    If "%%a" == "CT.TAB"   set CT.TAB=%%b
) 

echo. >> %LogFile%
echo. >> %LogFile%
echo =============================== >> %LogFile%
echo Feature Byte: >> %LogFile%
echo =============================== >> %LogFile%
echo SEG.ENT  : %SEG.ENT% >> %LogFile%
echo SEG.SBSO : %SEG.SBSO% >> %LogFile%
echo SEG.CON  : %SEG.CON% >> %LogFile%
echo SEG.PRM  : %SEG.PRM% >> %LogFile%
echo SEG.WKS  : %SEG.WKS% >> %LogFile%
echo SEG.RPS  : %SEG.RPS% >> %LogFile%
echo SEG.TC   : %SEG.TC% >> %LogFile%
echo CT.AIO   : %CT.AIO% >> %LogFile%
echo CT.MT    : %CT.MT% >> %LogFile%
echo CT.TC    : %CT.TC% >> %LogFile%
echo. >> %LogFile%
echo. >> %LogFile%

echo. >> %LogFile%
echo. >> %LogFile%
echo =============================== >> %LogFile%
echo Variable Matrix: >> %LogFile%
echo =============================== >> %LogFile%
echo BrandName         : %BRAND%  >> %LogFile%
echo OSSKU             : %OSSKU% >> %LogFile%
echo Family            : %FamilyCode%  >> %LogFile%
echo ImageID           : %ImageID% >> %LogFile%
echo OSFlag            : %OSFlag% >> %LogFile%
echo OfficeOA3         : %OfficeOA3% >> %LogFile%
echo SKUOptionConfig   : %SKUOP% >> %LogFile%
echo cNB/cDT detection : %mlPrefix% >> %LogFile%
echo. >> %LogFile%
echo. >> %LogFile%


echo =================== >> %LogFile%
echo   Setup Bounty Code >> %LogFile%
echo =================== >> %LogFile%

if /i "%mlPrefix%" == "CNB" GOTO CNBBOUNTY

:CDTBOUNTY
if /i "%BRAND%" == "Envy" set BountyCode=7015
if /i "%BRAND%" == "Pavilion" set BountyCode=7015
if /i "%BRAND%" == "OMEN" set BountyCode=7015
if /i "%SEG.PRM%" == "1" set BountyCode=7017
if /i "%SEG.SBSO%" == "1" set BountyCode=7023
if /i "%SEG.WKS%" == "1" set BountyCode=7022
if /i "%SEG.ENT%" == "1" set BountyCode=7020
if /i "%BRAND%" == "HPC" set BountyCode=7027
if /i "%BRAND%" == "Presario" set BountyCode=7027
if /i "%BRAND%" == "Compaq" set BountyCode=7027
if /i "%BountyCode%" == "" set BountyCode=7015
GOTO Install

:CNBBOUNTY
if /i "%BRAND%" == "Envy" set BountyCode=7016
if /i "%BRAND%" == "Pavilion" set BountyCode=7016
if /i "%BRAND%" == "OMEN" set BountyCode=7016
if /i "%BRAND%" == "HP Mini" set BountyCode=7016
if /i "%BRAND%" == "Compaq Mini" set BountyCode=7016
if /i "%BRAND%" == "Premium" set BountyCode=7018
if /i "%BRAND%" == "Compaq" set BountyCode=7019
if /i "%BRAND%" == "HPB" set BountyCode=7019
if /i "%BRAND%" == "Premium" if /i "%FamilyCode%" == "AN" set BountyCode=7019
if /i "%SEG.SBSO%" == "1" if /i "%CT.NB%" == "1" set BountyCode=7019
if /i "%SEG.ENT%" == "1" set BountyCode=7019
if /i "%BRAND%" == "Presario" set BountyCode=7028
if /i "%BRAND%" == "HPC" set BountyCode=7028
if /i "%OfficeOA3%" == "2" set BountyCode=7028
if /i "%BountyCode%" == "" set BountyCode=7016

:Install
echo BountyCode = %BountyCode% >> %LogFile%
echo. >> %LogFile%
echo. >> %LogFile%
echo =================== >> %LogFile%
echo   Detect AFO/ non-AFO >> %LogFile%
echo =================== >> %LogFile%
echo Start from OPK16.1, all should change to OEMTA in order to support late binding. >> %LogFile%
echo. >> %LogFile%

set AUTHTYPE=OEMTA


if /i "%SKUOP%" == "ABJ" (
	echo Japan, will use OEMPIPC  >> %LogFile%
	set AUTHTYPE=OEMPIPC
	echo XCOPY /cheirky "%~dp0favicon_office.ico" "C:\Windows\System32" >> %LogFile%
	XCOPY /cheirky "%~dp0favicon_office.ico" "C:\Windows\System32" >> %LogFile%%
	echo XCOPY /cheirky "%~dp0Regular" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs" >> %LogFile%
	XCOPY /cheirky "%~dp0Regular" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs" >> %LogFile%
	goto Commit
)


if /i "%SKUOP%" == "ACF" (
	echo Japan, will use OEMPIPC  >> %LogFile%
	set AUTHTYPE=OEMPIPC
	echo XCOPY /cheirky "%~dp0favicon_office.ico" "C:\Windows\System32" >> %LogFile%
	XCOPY /cheirky "%~dp0favicon_office.ico" "C:\Windows\System32" >> %LogFile%%
	echo XCOPY /cheirky "%~dp0Regular" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs" >> %LogFile%
	XCOPY /cheirky "%~dp0Regular" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs" >> %LogFile%
	goto Commit
)


if not exist "C:\system.sav\flags\Win7.flg" (
set AUTHTYPE=OEMTA
GOTO Commit
)
if /i "%OfficeOA3%" == "0" (
	echo Windows 7, Normal authentication method  >> %LogFile%
	set AUTHTYPE=OEM
)




:Commit
echo call oemsetup.cmd Mode=%AUTHTYPE% Referral=%BountyCode% >> %LogFile%
call oemsetup.cmd Mode=%AUTHTYPE% Referral=%BountyCode% >> %LogFile%
echo ErrorNum[%ERRORLEVEL%] >>%LogFile%
echo Copy logfile to System.sav/logs
echo copy %temp%\oobe.log C:\System.sav\logs\MSOffice16_MSlog.txt >> %LogFile%
copy %temp%\oobe.log C:\System.sav\logs\MSOffice16_MSlog.txt >> %LogFile%
echo Move PBR script >> %LogFile%
move /y oemsetupRecovery.cmd oemsetupRecovery.%AUTHTYPE%.%BountyCode%.cmd
dir >>%LogFile%
copy oemsetup.%AUTHTYPE%.%BountyCode%.cmd %~d0\Recovery\OEM\Point_B
copy oemsetup.%AUTHTYPE%.%BountyCode%.cmd %~d0\Recovery\OEM\Point_D
copy oemsetup.%AUTHTYPE%.%BountyCode%.cmd %~d0\system.sav\logs\MSOfficeoemsetup.%AUTHTYPE%.%BountyCode%.cmd
echo dir /b %~d0\Recovery\OEM\Point_B >>%LogFile%
dir /b %~d0\Recovery\OEM\Point_B >>%LogFile%
echo. >>%LogFile%
echo dir /b %~d0\Recovery\OEM\Point_D >>%LogFile%
dir /b %~d0\Recovery\OEM\Point_D >>%LogFile%

:Check
echo reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\16.0\Common\OEM >> %LOGFILE%
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\16.0\Common\OEM >> %LOGFILE%
echo reg query HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Office\16.0\Common\OEM >> %LOGFILE%
reg query HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Office\16.0\Common\OEM >> %LOGFILE%

GOTO END

:END
echo Finished.. >> %LogFile%
popd