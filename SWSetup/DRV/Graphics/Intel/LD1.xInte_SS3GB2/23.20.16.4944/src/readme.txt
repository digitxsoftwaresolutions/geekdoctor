Driver Version: 23.20.16.4944
 
Release Version: Production Version


Operating System(s):   

	Microsoft Windows* 10-64 

Platforms:
	6th Gen Intel(R) Core(TM) processor family (Codename Skylake)
	7th Gen Intel(R) Core(TM) processor family (Codename Kaby Lake)
	8th Gen Intel(R) Core(TM) processor family (codename Coffee Lake)
	
	
Release Date: February 8, 2018


CONTENTS

I.	Product Support
II.	Installation Information
III.	Disclaimer


I.  Product Support


Supports Intel(R) UHD Graphics, Intel(R) UHD Graphics, Intel(R) HD Graphics, Intel(R) Iris(R) Plus Graphics, Intel(R) Iris(R) Pro Graphics & Intel(R) HD Graphics on:

	6th Gen Intel(R) Core(TM) processor family (Codename Skylake)
	7th Gen Intel(R) Core(TM) processor family (Codename Kaby Lake)
	8th Gen Intel(R) Core(TM) processor family (codename Coffe Lake)
	
	
	
II.  Installation Information      

This document makes references to products developed by Intel. There are some restrictions on how these products may be used, and what information may be disclosed to others. Please read the Disclaimer section and contact your Intel field representative if you would like more information.

III.  Disclaimer

Intel is making no claims of usability, efficacy or warranty. The INTEL SOFTWARE LICENSE AGREEMENT contained herein completely defines the license and use of this software.

This document contains information on products in the design phase of development. The information here is subject to change without notice. Do not finalize a design with this information.


*Other names and brands may be claimed as the property of others.
