import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mybixbiteapp/AllScreens/budgetScreen.dart';
import 'package:mybixbiteapp/AllScreens/gratitudeScreen.dart';
import 'package:mybixbiteapp/AllScreens/laughScreen.dart';
import 'package:mybixbiteapp/AllScreens/learnScreen.dart';
import 'package:mybixbiteapp/AllScreens/liveScreen.dart';
import 'package:mybixbiteapp/AllScreens/loginScreen.dart';
import 'package:mybixbiteapp/AllScreens/loveScreen.dart';
import 'package:mybixbiteapp/AllScreens/mainscreen.dart';
import 'package:mybixbiteapp/AllScreens/openingScreen.dart';
import 'package:mybixbiteapp/AllScreens/registrationScreen.dart';
import 'package:mybixbiteapp/AllScreens/resultScreen.dart';
import 'package:mybixbiteapp/AllScreens/searchScreen.dart';
import 'package:mybixbiteapp/AllScreens/settingScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

DatabaseReference usersRef =
    FirebaseDatabase.instance.reference().child("users");

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: "Brand Bold",
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: OpeningScreen.idScreen,
      routes: {
        RegistrationScreen.idScreen: (context) => RegistrationScreen(),
        OpeningScreen.idScreen: (context) => OpeningScreen(),
        LoginScreen.idScreen: (context) => LoginScreen(),
        MainScreen.idScreen: (context) => MainScreen(),
        GratitudeScreen.idScreen: (context) => GratitudeScreen(),
        LiveScreen.idScreen: (context) => LiveScreen(),
        LaughScreen.idScreen: (context) => LaughScreen(),
        LoveScreen.idScreen: (context) => LoveScreen(),
        LearnScreen.idScreen: (context) => LearnScreen(),
        BudgetScreen.idScreen: (context) => BudgetScreen(),
        SearchScreen.idScreen: (context) => SearchScreen(),
        ResultScreen.idScreen: (context) => ResultScreen(),
        SettingScreen.idScreen: (context) => SettingScreen(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
