import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mybixbiteapp/AllScreens/gratitudeScreen.dart';
import 'package:mybixbiteapp/AllScreens/mainscreen.dart';
import 'package:mybixbiteapp/AllScreens/openingScreen.dart';
import 'package:mybixbiteapp/AllScreens/registrationScreen.dart';
import 'package:mybixbiteapp/AllWidgets/progressDialog.dart';
import 'package:mybixbiteapp/main.dart';


class ResetScreen extends StatefulWidget {
  @override
  _ResetScreenState createState() => _ResetScreenState();
}

class _ResetScreenState extends State<ResetScreen> {
  String _email;
  final auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Reset Password'),),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  hintText: 'Email'
              ),
              onChanged: (value) {
                setState(() {
                  _email = value.trim();
                });
              },
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              RaisedButton(
                child: Text('Send Request'),
                onPressed: () {
                  if(!_email.contains("@"))
                  {
                    displayToastMessage("Email address is not valid.", context);
                  }
                  else
                    {
                      auth.sendPasswordResetEmail(email: _email);
                      Navigator.of(context).pop();
                      displayToastMessage("Password Reset Sent...", context);
                    } },
                color: Theme.of(context).accentColor,
              ),

            ],
          ),

        ],),
    );
  }
}