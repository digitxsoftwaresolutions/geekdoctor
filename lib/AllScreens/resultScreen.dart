import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mybixbiteapp/AllScreens/liveScreen.dart';
import 'package:mybixbiteapp/AllScreens/loginScreen.dart';
import 'package:mybixbiteapp/AllScreens/loveScreen.dart';
import 'package:mybixbiteapp/AllScreens/mainscreen.dart';
import 'package:mybixbiteapp/AllScreens/registrationScreen.dart';
import 'package:mybixbiteapp/AllScreens/searchScreen.dart';
import 'package:mybixbiteapp/AllWidgets/Divider.dart';
import 'package:mybixbiteapp/AllWidgets/progressDialog.dart';
import 'package:mybixbiteapp/main.dart';

class ResultScreen extends StatelessWidget {
  static const String idScreen = "result";
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 25.0,
              ),
              Image(
                image: AssetImage("images/geek.png"),
                width: 150.0,
                height: 150.0,
                alignment: Alignment.center,
              ),
              SizedBox(
                height: 1.0,
              ),
              Divider(
                color: Colors.black,
                height: 50,
                thickness: 5,
              ),
              SizedBox(
                height: 1.0,
              ),
              Row(
                children: [
                  Icon(
                    Icons.volunteer_activism,
                    color: Colors.black,
                    size: 54,
                  ),
                  SizedBox(
                    width: 12.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Practice Gratitude",
                        style:
                            TextStyle(fontSize: 20.0, fontFamily: "Brand Bold"),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      Text(
                        "Your Gratitude.",
                        style: TextStyle(color: Colors.black54, fontSize: 12.0),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              DividerWidget(),
              SizedBox(
                height: 16.0,
              ),
              Row(
                children: [
                  Icon(
                    Icons.self_improvement,
                    color: Colors.black,
                    size: 54,
                  ),
                  SizedBox(
                    width: 12.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Take care of You",
                        style:
                            TextStyle(fontSize: 20.0, fontFamily: "Brand Bold"),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      Text(
                        "Taking Care.",
                        style: TextStyle(color: Colors.black54, fontSize: 12.0),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              DividerWidget(),
              SizedBox(
                height: 16.0,
              ),
              Row(
                children: [
                  Icon(
                    Icons.supervisor_account,
                    color: Colors.black,
                    size: 54,
                  ),
                  SizedBox(
                    width: 12.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Laugh with Friends",
                        style:
                            TextStyle(fontSize: 20.0, fontFamily: "Brand Bold"),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      Text(
                        "Friendship.",
                        style: TextStyle(color: Colors.black54, fontSize: 12.0),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              DividerWidget(),
              SizedBox(
                height: 16.0,
              ),
              Row(
                children: [
                  Icon(
                    Icons.sports_kabaddi,
                    color: Colors.black,
                    size: 54,
                  ),
                  SizedBox(
                    width: 12.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Nurture Relationships",
                        style:
                            TextStyle(fontSize: 20.0, fontFamily: "Brand Bold"),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      Text(
                        "Relationships.",
                        style: TextStyle(color: Colors.black54, fontSize: 12.0),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              DividerWidget(),
              SizedBox(
                height: 16.0,
              ),
              Row(
                children: [
                  Icon(
                    Icons.attractions,
                    color: Colors.black,
                    size: 54,
                  ),
                  SizedBox(
                    width: 12.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Continue Learning",
                        style:
                            TextStyle(fontSize: 20.0, fontFamily: "Brand Bold"),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      Text(
                        "Learn more.",
                        style: TextStyle(color: Colors.black54, fontSize: 12.0),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              DividerWidget(),
              SizedBox(
                height: 16.0,
              ),
              Row(
                children: [
                  Icon(
                    Icons.attach_money_outlined,
                    color: Colors.black,
                    size: 54,
                  ),
                  SizedBox(
                    width: 12.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Manage Your Money",
                        style:
                            TextStyle(fontSize: 20.0, fontFamily: "Brand Bold"),
                      ),
                      SizedBox(
                        height: 4.0,
                      ),
                      Text(
                        "Money.",
                        style: TextStyle(color: Colors.black54, fontSize: 12.0),
                      ),
                    ],
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: new Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      MaterialButton(
                        onPressed: () {
                          // Navigate to the second screen using a named route.
                          Navigator.pushNamed(context, MainScreen.idScreen);
                        },
                        color: Colors.white60,
                        child: Icon(
                          Icons.home,
                          color: Colors.blueAccent,
                          size: 50,
                        ),
                        shape: CircleBorder(),
                      ),
                      MaterialButton(
                        onPressed: () {
                          // Navigate to the second screen using a named route.
                          Navigator.pushNamed(context, SearchScreen.idScreen);
                        },
                        color: Colors.white60,
                        child: Icon(
                          Icons.search,
                          color: Colors.lightBlue,
                          size: 50,
                        ),
                        shape: CircleBorder(),
                      ),
                      MaterialButton(
                        onPressed: () {
                          // Navigate to the second screen using a named route.
                          Navigator.pushNamed(context, ResultScreen.idScreen);
                        },
                        color: Colors.white60,
                        child: Icon(
                          Icons.favorite,
                          color: Colors.red,
                          size: 50,
                        ),
                        shape: CircleBorder(),
                      ),
                      MaterialButton(
                        onPressed: () {
                          // Navigate to the second screen using a named route.
                          Navigator.pushNamed(context, ResultScreen.idScreen);
                        },
                        color: Colors.white60,
                        child: Icon(
                          Icons.settings,
                          color: Colors.grey,
                          size: 50,
                        ),
                        shape: CircleBorder(),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
