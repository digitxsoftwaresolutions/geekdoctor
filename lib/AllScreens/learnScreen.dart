import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mybixbiteapp/AllScreens/budgetScreen.dart';
import 'package:mybixbiteapp/AllScreens/liveScreen.dart';
import 'package:mybixbiteapp/AllScreens/loginScreen.dart';
import 'package:mybixbiteapp/AllScreens/mainscreen.dart';
import 'package:mybixbiteapp/AllScreens/registrationScreen.dart';
import 'package:mybixbiteapp/AllWidgets/progressDialog.dart';
import 'package:mybixbiteapp/main.dart';

class LearnScreen extends StatelessWidget {
  static const String idScreen = "learn";
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 35.0,
              ),
              Image(
                image: AssetImage("images/geek.png"),
                width: 150.0,
                height: 150.0,
                alignment: Alignment.center,
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                "Did you \n Continue \n Learning?",
                style: TextStyle(fontSize: 30.0, fontFamily: "Brand Bold"),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.all(50.0),
                child: new Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      MaterialButton(
                        onPressed: () {
                          // Navigate to the second screen using a named route.
                          Navigator.of(context).pop();
                        },
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Text('Back'),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                      MaterialButton(
                        onPressed: () {
                          // Navigate to the second screen using a named route.
                          Navigator.pushNamed(context, BudgetScreen.idScreen);
                        },
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Text('No'),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                      MaterialButton(
                        onPressed: () {
                          // Navigate to the second screen using a named route.
                          Navigator.pushNamed(context, BudgetScreen.idScreen);
                        },
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Text('Yes'),
                        padding: EdgeInsets.all(25),
                        shape: CircleBorder(),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
