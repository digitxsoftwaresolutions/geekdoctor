import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mybixbiteapp/AllScreens/loginScreen.dart';
import 'package:mybixbiteapp/AllScreens/mainscreen.dart';
import 'package:mybixbiteapp/AllScreens/registrationScreen.dart';
import 'package:mybixbiteapp/AllWidgets/progressDialog.dart';
import 'package:mybixbiteapp/main.dart';

class OpeningScreen extends StatelessWidget {
  static const String idScreen = "opening";
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 120.0, horizontal: 8.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 35.0,
              ),
              Image(
                image: AssetImage("images/geek.png"),
                width: 390.0,
                height: 250.0,
                alignment: Alignment.center,
              ),
              SizedBox(
                height: 1.0,
              ),
              Text(
                "",
                style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
                textAlign: TextAlign.center,
              ),
              new Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    ElevatedButton(
                      onPressed: () {
                        // Navigate to the second screen using a named route.
                        Navigator.pushNamed(
                            context, RegistrationScreen.idScreen);
                      },
                      child: Text('Sign up'),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        // Navigate to the second screen using a named route.
                        Navigator.pushNamed(context, LoginScreen.idScreen);
                      },
                      child: Text('Sign in'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  void loginAndAuthenticateUser(BuildContext context) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return ProgressDialog(
            message: "Authenticating, Please wait...",
          );
        });

    final User firebaseUser = (await _firebaseAuth
            .signInWithEmailAndPassword(
      email: emailTextEditingController.text,
      password: passwordTextEditingController.text,
    )
            .catchError((errMsg) {
      Navigator.pop(context);
      displayToastMessage("Error: " + errMsg.toString(), context);
    }))
        .user;

    if (firebaseUser != null) //user HttpStatus.created
    {
      usersRef.child(firebaseUser.uid).once().then((DataSnapshot snap) {
        if (snap.value != null) {
          Navigator.pushNamedAndRemoveUntil(
              context, MainScreen.idScreen, (route) => false);

          displayToastMessage("Welcome to MyBixbite.", context);
        } else {
          Navigator.pop(context);

          _firebaseAuth.signOut();

          displayToastMessage(
              "No record exists for this user, Please create new account.",
              context);
        }
      });
    } else {
      Navigator.pop(context);

      displayToastMessage("Error Occured.", context);
    }
  }
}
